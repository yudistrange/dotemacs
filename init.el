;; init.el

;;; Load the configurations present in the `~/.emacs.d/config.org` file
(add-hook 'after-init-hook
          `(lambda ()
             (require 'org)
             (org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))))

;; init.el ends here
